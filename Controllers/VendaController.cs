using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using teste_final_payment_api.Models;

namespace teste_final_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        static List<Venda> listaVendas = new List<Venda>();
        static int count = 0;

        public VendaController() { }

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            if (id > listaVendas.Count || id <=0)
                return NotFound();
            var venda = listaVendas[id-1];
            
            return Ok(venda);
        }

        [HttpGet("VerTodasVendas")]
        public IActionResult VerTodasVendas()
        {
            return Ok(listaVendas);
        }
        
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Produtos.Count == 0)
                return BadRequest(new { Erro = "Adicione pelo menos um produto para concluir a venda" });
            count++;
            venda.IdVenda = count;
            venda.Status = 0;
            listaVendas.Add(venda);

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.IdVenda }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, EnumStatusVenda status)
        {
            if (id > listaVendas.Count || id <=0)
                return BadRequest(new { Erro = "Venda inexistente. Escolha um ID válido." });
            if (listaVendas[id-1].Status == EnumStatusVenda.Aguardando_pagamento){
                if (status == EnumStatusVenda.Pagamento_aprovado || status == EnumStatusVenda.Cancelada){
                    listaVendas[id-1].Status = status;
                }
                else { 
                    return BadRequest(new { Erro = "O status não pode ser alterado para o requisitado. Escolha Pagamento_aprovado ou Cancelada." });
                }
            }
            else if (listaVendas[id-1].Status == EnumStatusVenda.Pagamento_aprovado){
                if (status == EnumStatusVenda.Enviado_para_transportadora || status == EnumStatusVenda.Cancelada){
                    listaVendas[id-1].Status = status;
                }
                else { 
                    return BadRequest(new { Erro = "O status não pode ser alterado para o requisitado. Escolha Enviado_para_transportadora ou Cancelada." });
                }
            }
            else if (listaVendas[id-1].Status == EnumStatusVenda.Enviado_para_transportadora){
                if (status == EnumStatusVenda.Entregue){
                    listaVendas[id-1].Status = status;
                }
                else { 
                    return BadRequest(new { Erro = "O status não pode ser alterado para o requisitado. Escolha Entregue." });
                }
            }
            else { 
                return BadRequest(new { Erro = "O status atual não pode ser alterado." });
            }

            return Ok(listaVendas[id-1]);
        }
       
    }
}