namespace teste_final_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada

    }
}