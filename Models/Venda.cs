using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace teste_final_payment_api.Models
{
    public class Venda
    {
        public int IdVenda { get; set; }
        public Vendedor VendedorId { get; set; }
        public List<string> Produtos { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}